var React    = require('react')
  , Phrases  = require('./phrases')

var Block = React.createClass({
  render: function(){
    return (
      <div className="w100-block">
        {this.props.children}
        <p className="w100-caption">{Phrases[this.props.attr]}</p>
      </div>
    )
  }
})

var Hero = React.createClass({
  render: function(){
    return (
      <div className="w100-hero">
        {this.props.children}
        <p className="w100-caption">{Phrases[this.props.attr]}</p>
      </div>
    )
  }
})

module.exports = {Block, Hero}
