var React             = require('react')
  , Graphics          = require('./graphics')
  , Blocks            = require('./blocks')
  , Pie               = Graphics.Pie
  , Line              = Graphics.Line
  , Num               = Graphics.Num
  , Time              = Graphics.Time
  , Block             = Blocks.Block
  , Hero              = Blocks.Hero


var Infographic = React.createClass({
  render: function(){
    
    return (
      <div>
        <Hero attr={"totalRecipients"}>
          <Line data={this.props.weeklys} attr={"totalRecipients"} />
        </Hero>
        <Block attr={"sendWeekday"}>
          <Pie data={this.props.weeklys} attr={"sendWeekday"} />
        </Block>
        <Block attr={"totalClicks"}>
          <Num data={this.props.weeklys} attr={"totalClicks"} fn={"count"} />
        </Block>
        <Hero attr={"campaignCost"}>
          <Line data={this.props.weeklys} attr={"campaignCost"} />
        </Hero>
        <Block attr={"openRate"}>
          <Num data={this.props.weeklys} attr={"openRate"} fn={"max"} />
        </Block>
        <Block attr={"timeOnSite"}>
          <Time data={this.props.weeklys} attr={"timeOnSite"} />
        </Block>
        <Hero attr={"newVisits"}>
          <Line data={this.props.weeklys} attr={"newVisits"} />
        </Hero>
      </div>
    )
  }
  
})

module.exports = Infographic
