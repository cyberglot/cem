var Phrases = {}

Phrases["totalRecipients"] = 'Número de pessoas inscritas'
Phrases["sendWeekday"] = 'Dias da semana que as campanhas foram enviadas'
Phrases["totalClicks"] = 'Número total de cliques em links durante as 99 edições'
Phrases["campaignCost"] = 'Custo em dolares por campanha'
Phrases["openRate"] = '% de emails abertos pelos inscritos'
Phrases["timeOnSite"] = 'Tempo gasto lendo todas as 99 edições'
Phrases["newVisits"] = 'Novos inscritos por edição'

module.exports = Phrases
