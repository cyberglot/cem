var React             = require('react')
  , Heart             = require('made-with-heart')
  , Infographic       = require('./info')
  , data              = require('./data').reverse()

require('./polyfill')

React.render(<Infographic weeklys={data} />, document.querySelector("section"))
React.render(
  <Heart made={"</>"} animation={['hrt-beating']} authors={[
             {name: 'Ju Gonçalves', link: 'http://jugoncalv.es'},
             {name: 'Willian Justen', link: 'http://willianjusten.com.br/'}
             ]} />,
  document.querySelector(".by"))
