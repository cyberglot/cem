var React             = require('react')
  , ChartistGraph     = require('react-chartist')

var Pie = React.createClass({
  
  options: {
    high: 10,
    low: -10,
    axisX: {
      labelInterpolationFnc: (value) => value[0]
    }
  }
  
  , responsiveOptions: [
    ['screen and (min-width: 640px)', {
      chartPadding: 30,
      labelOffset: 100,
      labelDirection: 'explode',
      labelInterpolationFnc: function(value) {
        return value;
      }
    }],
    ['screen and (min-width: 1024px)', {
      labelOffset: 50,
      chartPadding: 20
    }]
  ]
  
  , getData: (data, key) => {
    return data
      .map((w) => w[key])
      .reduce((data, key) => {
        let index = data.labels.findIndex((k) => k == key)

        if(index > -1){
          data.series[index] += 1
        }
        else {
          data.labels.push(key)
          data.series[data.series.length] = 1
        }

        return data
      }, {labels: [], series: []})
  }
  
  , render: function(){
    
    let data = this.getData(this.props.data, this.props.attr)
    
    return (
      <div className="w100-graphic">
        <div className="ct-chart">
          <ChartistGraph data={data} options={this.options} responsiveOptions={this.responsiveOptions} type={'Pie'} />
        </div>
      </div>
    )
  }
  
})

var Line = React.createClass({
  
  options: {
    axisX: {
      labelInterpolationFnc: (value, index) => index % 12 == 0 ? value : null
    }
  }
  
  , getData: (data, key) => {
    return data
      .map((w) => w[key].replace('$', ''))
      .reduce((data, key, index) => {
      
        data.labels.push(index + 1)
        data.series[0].push(!!Number(key) && Number(key) != 0 ? Number(key): 0)
      
        return data
      }, {labels: [], series: [[]]})
  }
  
  , render: function(){
    
    let data = this.getData(this.props.data, this.props.attr)
    
    return (
      <div className="w100-graphic w100-be-white">
        <div className="ct-chart">
          <ChartistGraph data={data} options={this.options} type={'Line'} />
        </div>
      </div>
    )
  }
  
})

var Num = React.createClass({
  
    initialReducers: {
        count: 0
      , max  : -Infinity
    }
  
  , count: (data, content) => {
    return data + Number(content)
  }
  
  , max: (data, content) => {
    return data > content ? data : content 
  }
  
  , getData: function(data, key, fn) {
    return data
      .map((w) => w[key].replace('%', ''))
      .reduce(this[fn], this.initialReducers[fn])
  }
  
  , render: function(){
    let data = this.getData(this.props.data, this.props.attr, this.props.fn)
    
    return (
      <div className="w100-graphic">
        <div className="w100-number">
          {data}
        </div>
      </div>
    )
  }
})

var Time = React.createClass({
  
    getData: function(data, key) {
      return data
        .filter((w) => w[key] != 'n/a')
        .map((w) => this.getSeconds(w[key]))
        .reduce((sum, t) => sum + t, 0)
  }
  
  , getSeconds: (time) => {
    let t = time.split(':')
    return Number(t[0]) * 60 + Number(t[1])
  }
  
  , toTime: (sec) => {
    let hours   = Math.floor(sec / 3600)
    let minutes = Math.floor((sec - (hours * 3600)) / 60)
    let seconds = sec - (hours * 3600) - (minutes * 60)

    if (hours   < 10) hours   = "0" + hours
    if (minutes < 10) minutes = "0" + minutes
    if (seconds < 10) seconds = "0" + seconds
    
    return  hours + ':' + minutes + ':' + seconds
  }
  
  , render: function(){
    let data = this.toTime(this.getData(this.props.data, this.props.attr))
    
    return (
      <div className="w100-graphic">
        <div className="w100-date">
          {data}
        </div>
      </div>
    )
  }
})

module.exports = {Pie, Line, Num, Time}
